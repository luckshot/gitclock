package bolt

import (
	"bytes"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func TestListRange(t *testing.T) {
	db := tmpFile(t)

	t.Run("empty", func(t *testing.T) {
		b, err := db.List("")
		if err != nil {
			t.Fatal(err)
		}
		if b != nil {
			t.Fatal(b)
		}
	})

	items := []string{"-one", "-two", "-three", "-four", "-five"}

	for _, v := range items {
		key := []byte("2020-01-02" + v)
		val := []byte(v)
		db.Write(key, val)
	}

	for _, v := range items {
		key := []byte("2020-01-03" + v)
		val := []byte(v)
		db.Write(key, val)
	}

	for _, v := range items {
		key := []byte("2020-01-04" + v)
		val := []byte(v)
		db.Write(key, val)
	}

	t.Run("list", func(t *testing.T) {
		b, err := db.List("")
		if err != nil {
			t.Fatal(err)
		}
		if len(b) != 15 {
			t.Fatal(len(b))
		}
	})

	t.Run("listprefix", func(t *testing.T) {
		b, err := db.List("2020-01-02")
		if err != nil {
			t.Fatal(err)
		}
		if len(b) != 5 {
			t.Fatal(len(b))
		}
	})

	t.Run("range", func(t *testing.T) {
		b, err := db.Range("2020-01-02", "2020-01-04")
		if err != nil {
			t.Fatal(err)
		}
		if len(b) != 10 {
			t.Fatal(len(b))
		}
	})
}

func TestCRD(t *testing.T) {
	tm, _ := time.Now().MarshalBinary()
	want := []byte("test string")

	db := tmpFile(t)

	t.Run("write", func(t *testing.T) {
		err := db.Write(tm, want)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("read", func(t *testing.T) {
		data, err := db.Read(tm)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(data, want) {
			t.Fatalf("want: %q: got: %q", want, data)
		}
	})

	t.Run("delete", func(t *testing.T) {
		if err := db.Delete(tm); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("check", func(t *testing.T) {
		b, err := db.List("")
		if err != nil {
			t.Fatal(err)
		}
		if len(b) != 0 {
			t.Fatal(len(b))
		}
	})
}

func tmpFile(t *testing.T) BoltDB {
	t.Helper()
	path := filepath.Join(os.TempDir(), "bolt-test.db")

	db, err := Init(path, "test bucket")
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		os.Remove(path)
	})
	return db
}
