package bolt

import (
	"bytes"

	bolt "go.etcd.io/bbolt"
)

// boltDB implements the storer interface.
type BoltDB struct {
	*bolt.DB
	bucket []byte
}

// Init opens or creates the database file and activates the named bucket.
func Init(name, bucket string) (BoltDB, error) {
	b := []byte(bucket)
	db, err := bolt.Open(name, 0600, nil)
	if err != nil {
		return BoltDB{}, err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(b)
		return err
	})
	return BoltDB{db, b}, err
}

// Write adds an item to the database with the given key.
func (db BoltDB) Write(key, value []byte) error {
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(db.bucket)
		return b.Put(key, value)
	})
	return err
}

// Read returns a specified item by key.
func (db BoltDB) Read(key []byte) ([]byte, error) {
	var r []byte
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(db.bucket)
		v := b.Get(key)
		r = v
		return nil
	})
	return r, err
}

// Delete removes a key from a bucket.
func (db BoltDB) Delete(key []byte) error {
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(db.bucket)
		return b.Delete(key)
	})
	return err
}

// List returns the items with the given prefix.
func (db BoltDB) List(p string) ([][]byte, error) {
	var d [][]byte
	err := db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket(db.bucket).Cursor()
		prefix := []byte(p)

		for k, v := c.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = c.Next() {
			d = append(d, v)
		}
		return nil
	})
	return d, err
}

// Range returns the items starting with the prefix `s` until the prefix `e`.
func (db BoltDB) Range(s, e string) ([][]byte, error) {
	var d [][]byte
	min := []byte(s)
	max := []byte(e)

	err := db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket(db.bucket).Cursor()
		for k, v := c.Seek(min); k != nil && bytes.Compare(k, max) <= 0; k, v = c.Next() {
			d = append(d, v)
		}
		return nil
	})
	return d, err
}
