module gitlab.com/luckshot/gitclock

go 1.14

require (
	go.etcd.io/bbolt v1.3.4
	golang.org/x/sys v0.0.0-20200301025734-6b2465a0221e // indirect
)
