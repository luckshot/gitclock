package main

import (
	"flag"
	"fmt"
	"strings"
	"time"

	"gitlab.com/luckshot/gitclock/gitclock"
)

func run(gc gitclock.Clock, args []string) (string, error) {
	subcmds := map[string]command{
		"start":  doStart,
		"stop":   doStop,
		"update": doUpdate,
		"edit":   doEdit,
		"status": doStatus,
		"list":   doList,
		"csv":    doCSV,
		"total":  doTotal,
	}

	if len(args) == 0 {
		return gc.Status(), nil
	}

	c := args[0]
	a := args[1:]

	cmd, ok := subcmds[c]
	if !ok {
		flag.Usage()
		return "", nil
	}
	return cmd(gc, a)
}

func doUpdate(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("update", flag.ExitOnError)
	fs.Parse(args)

	if err := gc.Update(strings.Join(args, " ")); err != nil {
		return "", err
	}
	return gc.Status(), nil
}

func doList(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("list", flag.ExitOnError)
	var (
		fRange = fs.String("range", "day", "Date range for total [day|week|month]")
		fDate  = fs.String("d", "", "Date for range (yyyy-mm-dd)")
		fYest  = fs.Bool("yest", false, "Print list from yesterday")
	)
	fs.Parse(args)

	now := time.Now()
	if *fYest {
		now = now.AddDate(0, 0, -1)
	}

	if *fDate != "" {
		d, err := time.Parse(`2006-01-02`, *fDate)
		if err != nil {
			return "", err
		}
		now = d
	}

	switch *fRange {
	case "day":
		return gc.List(now.Format(`2006-01-02`))
	case "week":
		return gc.List(gitclock.Week(now))
	case "month":
		return gc.List(now.Format(`2006-01`))
	default:
		return "", fmt.Errorf("bad option")
	}
}

func doTotal(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("total", flag.ExitOnError)
	var (
		fRange = fs.String("range", "day", "Date range for total [day|week|month]")
		fDate  = fs.String("d", "", "Date for range (yyyy-mm-dd)")
	)
	fs.Parse(args)

	now := time.Now()
	if *fDate != "" {
		d, err := time.Parse(`2006-01-02`, *fDate)
		if err != nil {
			return "", err
		}
		now = d
	}

	switch *fRange {
	case "day":
		return gc.Total(now.Format(`2006-01-02`))
	case "week":
		return gc.Total(gitclock.Week(now))
	case "month":
		return gc.Total(now.Format(`2006-01`))
	default:
		return "", fmt.Errorf("bad option")
	}
}

func doCSV(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("csv", flag.ExitOnError)
	var (
		fRange  = fs.String("range", "day", "Date range for total [day|week|month]")
		fDate   = fs.String("d", "", "Date for range (yyyy-mm-dd)")
		fHeader = fs.Bool("header", false, "Print CSV header")
		fYest   = fs.Bool("yest", false, "Print CSV from yesterday")
	)
	fs.Parse(args)

	now := time.Now()
	if *fYest {
		now = now.AddDate(0, 0, -1)
	}

	if *fDate != "" {
		d, err := time.Parse(`2006-01-02`, *fDate)
		if err != nil {
			return "", err
		}
		now = d
	}

	switch *fRange {
	case "day":
		return gc.CSV(*fHeader, now.Format(`2006-01-02`))
	case "week":
		s, e := gitclock.Week(now)
		return gc.CSV(*fHeader, s, e)
	case "month":
		return gc.CSV(*fHeader, now.Format(`2006-01`))
	default:
		return "", fmt.Errorf("bad option")
	}
}

func doStatus(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("status", flag.ExitOnError)
	fs.Parse(args)

	return gc.Status(), nil
}

func doStart(gc gitclock.Clock, args []string) (string, error) {
	now := time.Now()
	fs := flag.NewFlagSet("start", flag.ExitOnError)
	var (
		fDate = fs.String("d", now.Format(`2006-01-02`), "Start date (yyyy-mm-dd)")
		fTime = fs.String("t", now.Format(`15:04`), "Start time (hh:mm)")
	)
	fs.Parse(args)

	start := fmt.Sprintf("%sT%s:00", *fDate, *fTime)
	t, err := time.ParseInLocation(`2006-01-02T15:04:05`, start, time.Local)
	if err != nil {
		return "", err
	}

	if err := gc.Start(t, strings.Join(fs.Args(), " ")); err != nil {
		return "", err
	}
	return gc.Status(), nil
}

func doStop(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("stop", flag.ExitOnError)
	fs.Parse(args)

	err := gc.Stop(time.Now())
	if err != nil {
		return "", err
	}
	return gc.Status(), nil
}
