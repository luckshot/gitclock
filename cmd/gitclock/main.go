package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/luckshot/gitclock/gitclock"
)

const help = `
  Commands:
	status Prints the running timers
	start  Starts a new timer, after stopping any running timers
	stop   Stops running timers
	update Updates the text of a running timer
	edit   Edit saved timers
	list   Prints the stopped timers in bucket
	csv    Prints the stopped timers in CSV format
	total  Prints the total hours
`

type command func(gitclock.Clock, []string) (string, error)

func init() {
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(), "Usage:")
		flag.PrintDefaults()
		fmt.Fprintln(flag.CommandLine.Output(), help)
	}
}

func main() {
	confDir, err := os.UserConfigDir()
	if err != nil {
		log.Println("home:", err)
		os.Exit(2)
	}
	confDir = filepath.Join(confDir, "gitclock.db")

	fBucket := flag.String("b", "main", "Database bucket to use")
	fDatabase := flag.String("db", confDir, "Database to use")
	flag.Parse()

	gc, err := gitclock.Init(*fDatabase, *fBucket)
	if err != nil {
		log.Println("gitclock:", err)
		os.Exit(3)
	}
	out, err := run(gc, os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print(out)
}
