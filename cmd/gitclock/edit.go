package main

import (
	"flag"
	"time"

	"gitlab.com/luckshot/gitclock/gitclock"
)

func doEdit(gc gitclock.Clock, args []string) (string, error) {
	fs := flag.NewFlagSet("edit", flag.ExitOnError)
	var (
		fDate = fs.String("d", time.Now().Format(`2006-01-02`), "Date for range (yyyy-mm-dd)")
	)
	fs.Parse(args)

	_, err := time.Parse(`2006-01-02`, *fDate)
	if err != nil {
		return "", err
	}
	return gc.Edit(*fDate)
}
