# GitClock

GitClock is a simple time-tracking utility. It has two parts, the client and the 
server.

## Installation

From the root of the repo, run `go install ./...`

Aliasing or symlinking `gitclock-cli` to `gc` is a convienient shortcut.

## Usage

### Server

The server can be launched in the background with `&`. Its options are:

```
$ gitclock-api -h
Usage of gitclock-api:
  -file file
        Output file (default "<home>/timelog.csv")
  -port port
        Server port (default ":42069")
```

### Client

The client controls the timer. The default, when no options are given, is to 
display the current timer.

It's options are:

```
$ gitclock-cli -h
Usage of gitclock-cli:
  -e    Stop the current timer
  -kill
        Shutdown the server
  -l    Log the timer data to file
  -s task
        Start a new timer, with task
  -sls
        Stop timer, log data, start new timer
  -u task
        Update the timer with task
```
