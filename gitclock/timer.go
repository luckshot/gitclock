package gitclock

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"strings"
	"time"
)

// Timer is the base type.
type timer struct {
	Start time.Time
	Stop  time.Time
	Text  string
}

// NewTimer returns a new started timer.
func newTimer(msg string, start time.Time) timer {
	t := timer{
		Start: start.Round(time.Second),
		Text:  msg,
	}
	return t
}

// Encode converts a timer to a binary blob.
func (t timer) Encode(enc io.Writer) error {
	return gob.NewEncoder(enc).Encode(t)
}

// Decode returns a timer from the binary blob.
func (t *timer) Decode(byt []byte) error {
	return gob.NewDecoder(bytes.NewBuffer(byt)).Decode(t)
}

// Key returns a byte slice for use as a database key. The key is
// the the RFC3339 date, prefixed with "running-" if the stop time
// is not set.
func (t timer) Key() []byte {
	if t.Start.IsZero() {
		return nil
	}
	key := t.Start.Format(time.RFC3339)

	if t.IsRunning() {
		key = runPrefix + key
	}
	return []byte(key)
}

// String implements the Stringer interface.
func (t timer) String() string {
	return fmt.Sprintf(`% s% 7s | %-35s | %s`,
		t.status(),
		t.duration(),
		t.text(),
		t.Start.Format(`15:04 Mon 02-Jan`),
	)
}

// IsRunning returns the state of the timer.
func (t timer) IsRunning() bool {
	return t.Stop.IsZero()
}

// Duration returns the elapsed duration of a timer.
func (t timer) Duration() float64 {
	if t.IsRunning() {
		return time.Now().Sub(t.Start).Round(3 * time.Minute).Hours()
	}
	return t.Stop.Sub(t.Start).Round(3 * time.Minute).Hours()
}

func (t timer) duration() string {
	var out string
	if t.IsRunning() {
		out = time.Now().Sub(t.Start).Round(time.Minute).String()
		if out == "0s" {
			return "0m"
		}
		return strings.TrimSuffix(out, "0s")
	}
	out = t.Stop.Sub(t.Start).Round(time.Minute).String()
	return strings.TrimSuffix(out, "0s")
}

func (t timer) status() string {
	if t.Stop.IsZero() {
		return "*"
	}
	return " "
}

func (t timer) text() string {
	l := 35
	if len(t.Text) > l {
		return t.Text[:l]
	}
	return t.Text
}
