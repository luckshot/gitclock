package gitclock

import (
	"encoding/csv"
	"fmt"
	"strings"
	"time"
)

// stop all running timers
func stopAll(c Clock, s time.Time) error {
	runs, err := c.list(runPrefix)
	if err != nil {
		return err
	}

	for _, v := range runs {
		if err := c.remove(v); err != nil {
			return err
		}
		v.Stop = s.Round(time.Minute)

		if err := c.write(v); err != nil {
			return err
		}
	}
	return nil
}

func calc(tmrs []timer) (string, error) {
	var dur float64
	for _, v := range tmrs {
		dur += v.Duration()
	}
	return fmt.Sprintf("%.2fh", dur), nil
}

func format(tmrs []timer) (string, error) {
	var buf strings.Builder
	var dur float64

	for _, v := range tmrs {
		buf.WriteString(v.String() + "\n")
		dur += v.Duration()
	}
	buf.WriteString("-----------------------------------------------------------------\n")
	fmt.Fprintf(&buf, "    Total: %.2f\n", dur)
	return buf.String(), nil
}

func formatCSV(tmrs []timer, h bool) (string, error) {
	f := time.RFC3339
	var buf strings.Builder
	c := csv.NewWriter(&buf)
	if h {
		c.Write([]string{"Start Time", "Stop Time", "Description"})
	}

	for _, v := range tmrs {
		c.Write([]string{v.Start.Format(f), v.Stop.Format(f), v.Text})
	}
	c.Flush()
	return buf.String(), nil
}

func Week(t time.Time) (string, string) {
	f := `2006-01-02`
	if t.Weekday() == time.Monday {
		return t.Format(f), t.AddDate(0, 0, 7).Format(f)
	}

	if t.Weekday() == time.Sunday {
		return t.AddDate(0, 0, -6).Format(f), t.AddDate(0, 0, 1).Format(f)
	}
	m := int(time.Monday - t.Weekday())
	return t.AddDate(0, 0, m).Format(f), t.AddDate(0, 0, m+7).Format(f)
}
