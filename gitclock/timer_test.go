package gitclock

import (
	"bytes"
	"fmt"
	"testing"
	"time"
)

func TestEncDec(t *testing.T) {
	now := time.Now()
	te := newTimer("A testing test.", now)
	te.Stop = (now.Add(time.Hour))

	var buf bytes.Buffer
	if err := te.Encode(&buf); err != nil {
		t.Fatal(err)
	}

	var de timer
	if err := de.Decode(buf.Bytes()); err != nil {
		t.Fatal(err)
	}

	if te.String() != de.String() {
		t.Fatalf("%q, %q", te, de)
	}
}

func TestKey(t *testing.T) {
	now := time.Now()

	t.Run("happy", func(t *testing.T) {
		tm := newTimer("testing, key", now)
		tm.Stop = now.Add(time.Hour)
		k := tm.Key()

		key := now.Round(time.Second).Format(time.RFC3339)

		if !bytes.Equal(k, []byte(key)) {
			t.Fatalf("%q, %q", k, []byte(key))
		}
	})

	t.Run("null", func(t *testing.T) {
		tm := timer{}
		if k := tm.Key(); k != nil {
			t.Fatal("mystery key")
		}
	})

	t.Run("running", func(t *testing.T) {
		tm := newTimer("test", now)
		k := tm.Key()
		if !bytes.HasPrefix(k, []byte(runPrefix)) {
			t.Fatalf("%s", k)
		}
	})
}

func TestStringStopped(t *testing.T) {
	now := time.Date(2020, time.January, 1, 12, 0, 0, 0, time.UTC)
	tm := newTimer("testing string", now)

	t.Run("short", func(t *testing.T) {
		tm.Stop = now.Add(time.Minute)
		s := tm.String()
		w := "      1m | testing string                      | 12:00 Wed 01-Jan"
		if s != w {
			t.Fatalf("\n%s\n%s", s, w)
		}
	})

	t.Run("long", func(t *testing.T) {
		tm.Stop = tm.Stop.Add(time.Hour)
		s := tm.String()
		w := "    1h1m | testing string                      | 12:00 Wed 01-Jan"
		if s != w {
			t.Fatalf("\n%s\n%s", s, w)
		}
	})

	t.Run("second", func(t *testing.T) {
		tm.Stop = tm.Stop.Add(30 * time.Second)
		s := tm.String()
		w := "    1h2m | testing string                      | 12:00 Wed 01-Jan"
		if s != w {
			t.Fatalf("\n%s\n%s", s, w)
		}
	})
}

func TestStringRunning(t *testing.T) {
	now := time.Now().Round(time.Minute)

	t.Run("minute", func(t *testing.T) {
		tm := newTimer("testing string to be longer that 35 characters", now)
		s := tm.String()
		w := fmt.Sprintf("*     0m | testing string to be longer that 35 | %s",
			now.Add(15*time.Second).Format(`15:04 Mon 02-Jan`))
		if s != w {
			t.Fatalf("\n%s\n%s", s, w)
		}
	})

	t.Run("second", func(t *testing.T) {
		n := now.Add(-15 * time.Minute)
		tm := newTimer("testing string to be longer that 35 characters", n)
		s := tm.String()
		w := fmt.Sprintf("*    15m | testing string to be longer that 35 | %s",
			n.Format(`15:04 Mon 02-Jan`))
		if s != w {
			t.Fatalf("\n%s\n%s", s, w)
		}
	})
}

// This will fail on the day after the DST change.
func TestDuration(t *testing.T) {
	// t.Skip()
	now := time.Now()
	start := now.AddDate(0, 0, -1)
	tmr := newTimer("testing", start)

	t.Run("running", func(t *testing.T) {
		want := float64(24.0)
		got := tmr.Duration()
		if got != want {
			t.Fatalf("%f", got)
		}
	})

	end := start.Add(30 * time.Minute)
	tmr.Stop = end
	t.Run("stopped", func(t *testing.T) {
		want := 0.5
		got := tmr.Duration()
		if got != want {
			t.Fatal(got)
		}
	})
}
