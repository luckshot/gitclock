package gitclock

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

// Edit allows editing of stored timers.
func (c Clock) Edit(d string) (string, error) {
	if _, err := time.Parse(`2006-01-02`, d); err != nil {
		return "", err
	}

	timers, err := c.list(d)
	if err != nil {
		return "", err
	}
	if timers == nil {
		return "No timers to edit!\n", nil
	}

	item, err := editList(timers)
	if err != nil {
		return "", err
	}

	fmt.Println("Enter new data, or * to keep current data...")
	newTimer, err := updateTimer(timers[item])
	if err != nil {
		return "", err
	}

	err = c.remove(timers[item])
	if err != nil {
		return "", err
	}
	err = c.write(newTimer)
	if err != nil {
		return "", err
	}

	return c.List(d)
}

func updateTimer(tmr timer) (timer, error) {
	var (
		start string
		stop  string
		msg   string
	)

	fmt.Printf("New start time (%s): ", tmr.Start.Format(`15:04`))
	_, err := fmt.Scanln(&start)
	if err != nil {
		return timer{}, err
	}
	newStart, err := parseTime(start, tmr.Start)
	if err != nil {
		return timer{}, err
	}

	fmt.Printf("New stop time (%s): ", tmr.Stop.Format(`15:04`))
	if _, err = fmt.Scanln(&stop); err != nil {
		return timer{}, err
	}
	newStop, err := parseTime(stop, tmr.Stop)
	if err != nil {
		return timer{}, err
	}

	fmt.Printf("New message (%s): ", tmr.Text)
	scanner := bufio.NewScanner(os.Stdin)
	if ok := scanner.Scan(); !ok {
		return timer{}, fmt.Errorf("scanner problem")
	}

	switch scanner.Text() {
	case "*":
		msg = tmr.Text
	default:
		msg = scanner.Text()
	}

	t := timer{
		Start: newStart,
		Stop:  newStop,
		Text:  msg,
	}
	return t, nil
}

func parseTime(s string, t time.Time) (time.Time, error) {
	if s == "*" {
		return t, nil
	}

	tm, err := time.Parse(`15:04`, s)
	if err != nil {
		return t, err
	}
	tm = time.Date(t.Year(), t.Month(), t.Day(), tm.Hour(), tm.Minute(), 0, 0, time.Local)
	return tm, nil
}

func editList(list []timer) (int, error) {
	for k, v := range list {
		fmt.Printf("% 2d:%s\n", k+1, v)
	}

	fmt.Print("Edit #: ")
	var item int
	_, err := fmt.Scanln(&item)
	if err != nil {
		return 0, err
	}

	if item <= 0 || item > len(list) {
		return 0, fmt.Errorf("invalid choice")
	}
	return item - 1, err
}
