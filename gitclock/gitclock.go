package gitclock

import (
	"fmt"
	"strings"
	"time"
)

const runPrefix = `running-`

type storer interface {
	write(timer) error
	remove(timer) error
	list(string) ([]timer, error)
	listRange(string, string) ([]timer, error)
}

// Clock holds the database connection and timer data.
type Clock struct {
	storer
}

// Init
func Init(name, bucket string) (Clock, error) {
	db, err := initBolt(name, bucket)
	if err != nil {
		return Clock{}, err
	}
	return Clock{db}, nil
}

// Start ends the latest running timer and saves a new started timer
// to the database.
func (c Clock) Start(start time.Time, msg string) error {
	start = start.Truncate(time.Minute)
	if err := stopAll(c, start); err != nil {
		return err
	}
	t := newTimer(msg, start)
	return c.write(t)
}

// Stop ends a running timer and saves it to the database.
func (c Clock) Stop(end time.Time) error {
	return stopAll(c, end)
}

func (c Clock) Update(msg string) error {
	timers, err := c.list(runPrefix)
	if err != nil {
		return err
	}

	if timers == nil {
		c.Start(time.Now(), msg)
		return nil
	}

	for _, v := range timers {
		v.Text = msg
		err := c.write(v)
		if err != nil {
			return err
		}
	}
	return nil
}

// Status returns the status of any running timers.
func (c Clock) Status() string {
	timers, err := c.list(runPrefix)
	if err != nil {
		return err.Error()
	}

	if timers == nil {
		return "No timers running\n"
	}

	var buf strings.Builder
	for _, v := range timers {
		fmt.Fprintf(&buf, "%s\n", v)
	}
	return buf.String()
}

// List returns a formatted list of timers
func (c Clock) List(prefix ...string) (string, error) {
	r, err := c.list(runPrefix)
	if err != nil {
		return "", err
	}

	switch len(prefix) {
	case 1:
		t, err := c.list(prefix[0])
		if err != nil {
			return "", err
		}
		return format(append(t, r...))

	case 2:
		t, err := c.listRange(prefix[0], prefix[1])
		if err != nil {
			return "", err
		}
		return format(append(t, r...))

	default:
		return "", fmt.Errorf("too many prefixes")
	}
}

// CSV returns a formatted list of timers
func (c Clock) CSV(header bool, prefix ...string) (string, error) {
	switch len(prefix) {
	case 1:
		t, err := c.list(prefix[0])
		if err != nil {
			return "", err
		}
		return formatCSV(t, header)

	case 2:
		t, err := c.listRange(prefix[0], prefix[1])
		if err != nil {
			return "", err
		}
		return formatCSV(t, header)

	default:
		return "", fmt.Errorf("too many prefixes")
	}
}

// Total returns the total duration of timers for the prefix or prefix
// range given.
func (c Clock) Total(prefix ...string) (string, error) {
	r, err := c.list(runPrefix)
	if err != nil {
		return "", err
	}

	switch len(prefix) {
	case 1:
		t, err := c.list(prefix[0])
		if err != nil {
			return "", err
		}
		return calc(append(t, r...))

	case 2:
		t, err := c.listRange(prefix[0], prefix[1])
		if err != nil {
			return "", err
		}
		return calc(append(t, r...))

	default:
		return "", fmt.Errorf("too many prefixes")
	}
}
