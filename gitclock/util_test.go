package gitclock

import (
	"testing"
	"time"
)

func TestWeek(t *testing.T) {
	table := []struct {
		date  time.Time
		start string
		end   string
	}{
		{time.Date(2019, time.December, 31, 12, 0, 0, 0, time.UTC), "2019-12-30", "2020-01-06"},
		{time.Date(2020, time.February, 28, 12, 0, 0, 0, time.UTC), "2020-02-24", "2020-03-02"},
		{time.Date(2020, time.March, 2, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 3, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 4, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 5, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 6, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 7, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
		{time.Date(2020, time.March, 8, 12, 0, 0, 0, time.UTC), "2020-03-02", "2020-03-09"},
	}

	for k, v := range table {
		s, e := Week(v.date)
		t.Run("start", func(t *testing.T) {
			if s != v.start {
				t.Fatalf("%d. start: got %q, want %q", k, s, v.start)
			}
		})

		t.Run("end", func(t *testing.T) {
			if e != v.end {
				t.Fatalf("%d. end: got %q, want %q", k, e, v.end)
			}
		})
	}
}
