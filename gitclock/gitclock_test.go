package gitclock

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

func TestStartStop(t *testing.T) {
	now := time.Now()
	clk := tmpFile(t)

	err := clk.Start(now, "test start 1")
	if err != nil {
		t.Fatal(err)
	}
	err = clk.Start(now, "test start 1")
	if err != nil {
		t.Fatal(err)
	}
	err = clk.Stop(now)
	if err != nil {
		t.Fatal(err)
	}
	err = clk.Stop(now)
	if err != nil {
		t.Fatal(err)
	}
}

func TestTotal(t *testing.T) {
	clk := tmpFile(t)

	str := clk.Status()
	if str != "No timers running\n" {
		t.Fatal(str)
	}

	times := []struct {
		start time.Time
		dur   string
	}{
		{time.Date(2020, time.January, 1, 12, 0, 0, 0, time.UTC), "1h"},
		{time.Date(2020, time.January, 2, 12, 0, 0, 0, time.UTC), "1h30m"},
		{time.Date(2020, time.January, 3, 12, 0, 0, 0, time.UTC), "1h15m"},
		{time.Date(2020, time.February, 3, 14, 0, 0, 0, time.UTC), "1h15m"},
		{time.Date(2020, time.February, 3, 12, 0, 0, 0, time.UTC), "1h"},
	}

	for k, v := range times {
		if err := clk.Start(v.start, fmt.Sprintf("test %d", k)); err != nil {
			t.Fatal(err)
		}
		d, _ := time.ParseDuration(v.dur)
		if err := clk.Stop(v.start.Add(d)); err != nil {
			t.Fatal(err)
		}
	}

	t.Run("bucket", func(t *testing.T) {
		str, err := clk.Total("")
		if err != nil {
			t.Fatal(err)
		}
		if str != "6.00h" {
			t.Fatal(str)
		}
	})

	t.Run("week", func(t *testing.T) {
		str, err := clk.Total("2020-01")
		if err != nil {
			t.Fatal(err)
		}
		if str != "3.75h" {
			t.Fatal(str)
		}
	})

	t.Run("day", func(t *testing.T) {
		str, err := clk.Total("2020-01-02")
		if err != nil {
			t.Fatal(err)
		}
		if str != "1.50h" {
			t.Fatal(str)
		}
	})

	t.Run("range", func(t *testing.T) {
		str, err := clk.Total("2020-01-01", "2020-01-03")
		if err != nil {
			t.Fatal(err)
		}
		if str != "2.50h" {
			t.Fatal(str)
		}
	})
}

func TestStatus(t *testing.T) {
	now := time.Now()
	clk := tmpFile(t)

	t.Run("not running", func(t *testing.T) {
		str := clk.Status()
		if str != "No timers running\n" {
			t.Fatal(str)
		}
	})

	t.Run("running", func(t *testing.T) {
		if err := clk.Start(now, "this is running"); err != nil {
			t.Fatal(err)
		}

		str := clk.Status()
		if !strings.Contains(str, "this is running") {
			t.Fatalf("%q", str)
		}
	})
}

func TestListing(t *testing.T) {
	now := time.Now()
	clk := tmpFile(t)

	t.Run("setup", func(t *testing.T) {
		if err := clk.Start(now.Add(-1*time.Hour), "this is running 1"); err != nil {
			t.Fatal(err)
		}
		if err := clk.Start(now, "this is running 2"); err != nil {
			t.Fatal(err)
		}
	})

	t.Run("empty", func(t *testing.T) {
		str, err := clk.List("")
		if err != nil {
			t.Fatal(err)
		}
		fmt.Print(str)
	})

	t.Run("single", func(t *testing.T) {
		str, err := clk.List(now.Format(`2006-01-02`))
		if err != nil {
			t.Fatal(err)
		}
		fmt.Print(str)
	})

	t.Run("range", func(t *testing.T) {
		str, err := clk.List(now.Format(`2006-01-02`), now.AddDate(0, 0, 1).Format(`2006-01-02`))
		if err != nil {
			t.Fatal(err)
		}
		fmt.Print(str)
	})
}

func TestUpdate(t *testing.T) {
	clk := tmpFile(t)
	want := "this is now updated"

	clk.Update("this is not updated")
	if err := clk.Update(want); err != nil {
		t.Fatal(err)
	}

	t.Run("update", func(t *testing.T) {
		w, err := clk.list(runPrefix)
		if err != nil {
			t.Fatal(err)
		}
		got := w[0].Text
		if got != want {
			t.Fatal(got)
		}
	})
}

func tmpFile(t *testing.T) Clock {
	t.Helper()
	path := filepath.Join(os.TempDir(), "gitclock.db")

	clk, err := Init(path, "test bucket")
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		os.Remove(path)
	})
	return Clock{clk}
}
