package gitclock

import (
	"bytes"

	"gitlab.com/luckshot/gitclock/internal/bolt"
)

type boltDBStore struct {
	bolt.BoltDB
}

func initBolt(path, bucket string) (boltDBStore, error) {
	db, err := bolt.Init(path, bucket)
	if err != nil {
		return boltDBStore{}, err
	}
	return boltDBStore{db}, nil
}

func (bdb boltDBStore) write(item timer) error {
	var buf bytes.Buffer
	if err := item.Encode(&buf); err != nil {
		return err
	}
	return bdb.BoltDB.Write(item.Key(), buf.Bytes())
}

func (bdb boltDBStore) remove(item timer) error {
	return bdb.BoltDB.Delete(item.Key())
}

func (bdb boltDBStore) list(p string) ([]timer, error) {
	byt, err := bdb.BoltDB.List(p)
	if err != nil {
		return nil, err
	}
	return convert(byt)
}

func (bdb boltDBStore) listRange(s, e string) ([]timer, error) {
	byt, err := bdb.BoltDB.Range(s, e)
	if err != nil {
		return nil, err
	}
	return convert(byt)
}

func convert(b [][]byte) ([]timer, error) {
	var tmrs []timer
	for _, v := range b {
		var t timer
		if err := t.Decode(v); err != nil {
			return nil, err
		}
		tmrs = append(tmrs, t)
	}
	return tmrs, nil
}
